<?php

/*
 * You can place your custom package configuration in here.
 */
return [

    'imageoptimaization' => [

        'img_quality' => env('CH_FEEDBACK_IMG_QUALITY', 90),

        'img_format' => env('CH_FEEDBACK_IMG_FORMAT', 'webp'),

        'avatar' => [
            ['width' => 200, 'height' => 200]
        ],

        'avatar_default_size' => '200x200'

    ]

];
