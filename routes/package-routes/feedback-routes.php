<?php

//feedback routes
Route::group(['name' => 'feedback', 'groupName' => 'feedback'], function () {
    Route::get('/feedback/grid', 'PluginsControllers\FeedbackController@grid')->name('feedbackGrid');

    Route::get('/feedback/get-feedback/{id}', 'PluginsControllers\FeedbackController@getFeedbackById');
    Route::get('/feedback/get-feedback-by-group/{group}',
        'PluginsControllers\FeedbackController@getFeedbackByGroup');
    Route::get('/feedback/get-all-feedback', 'PluginsControllers\FeedbackController@getAllFeedback');
    Route::get('/feedback/get-feedback-by-visibility/{visibility}',
        'PluginsControllers\FeedbackController@getAllFeedbackByVisibility');


    Route::resource('/feedback', 'PluginsControllers\FeedbackController', [
        'names' => [
            'index' => 'feedback'
        ]
    ]);
});