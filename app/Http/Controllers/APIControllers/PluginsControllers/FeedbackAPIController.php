<?php

namespace App\Http\Controllers\APIControllers\PluginsControllers;


/**
 * @group Feedback
 *
 * APIs for feedback
 */
class FeedbackAPIController extends \Creativehandles\ChFeedback\Http\Controllers\APIControllers\FeedbackAPIController
{
}
