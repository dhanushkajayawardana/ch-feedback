<?php

use Illuminate\Database\Migrations\Migration;

class transferRankToFeedbackTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // move the existing data into the translations table
        DB::statement(
            'update feedback_translations as t join feedbacks as m on t.feedback_id = m.id set t.rank = m.rank'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Move the translated data back into the main table. Translations of the default language of the application are selected.
        DB::statement(
            'update feedbacks as m join feedback_translations as t on m.id = t.feedback_id set m.rank = t.rank where t.locale = :locale',
            ['locale' => config('app.locale')]
        );
    }
}
