<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class translateRankInFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create the translations table
        Schema::table('feedback_translations', function (Blueprint $table) {
            $table->string('rank', 191)->nullable()->default(null)->after('locale');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feedback_translations', function (Blueprint $table) {
            $table->dropColumn('rank');
        });
    }
}
