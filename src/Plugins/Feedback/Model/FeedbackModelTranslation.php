<?php

namespace Creativehandles\ChFeedback\Plugins\Feedback\Model;

use Illuminate\Database\Eloquent\Model;

class FeedbackModelTranslation extends Model
{
    protected $table = 'feedback_translations';
    public $fillable = ['rank', 'text'];
}
