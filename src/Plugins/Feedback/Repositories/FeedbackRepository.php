<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 24/7/19
 * Time: 6:27 PM
 */

namespace Creativehandles\ChFeedback\Plugins\Feedback\Repositories;


use App\Repositories\Exceptions\EloquentRepositoryException;
use Creativehandles\ChFeedback\Plugins\Feedback\Model\FeedbackModel;
use App\Repositories\BaseEloquentRepository;
use Illuminate\Database\Eloquent\Model;

class FeedbackRepository extends BaseEloquentRepository
{
    /**
     * Get the model to be used for the repository;
     *
     * @return FeedbackModel
     */
    public function getModel()
    {
        return new FeedbackModel();
    }

    /**
     * Returns a data array needed for JS DataTables plugin
     *
     * @param array $searchColumns Columns to use for searching
     * @param array $orderColumns Array of columns to order
     * @param string $defOrderColumnKey Key for a column in $orderColumns array to be used as fallback column to order
     * @param array $with Array of Eloquent relationships to eager load.
     * @param array $where where filter ['column,'condition','value']
     *
     * @return array
     * @throws EloquentRepositoryException
     */
    public function getDataforDataTables(array $searchColumns = null, array $orderColumns = null, string $defOrderColumnKey = null, array $with = null, array $where= null)
    {
        $orderColumn = null;
        $dir = request()->input('order.0.dir', 'asc');
        $selectCols = [
            'feedbacks.id',
            'feedbacks.rating',
            'feedbacks.user_id',
            'feedbacks.visibility',
            'feedbacks.deleted_by',
            'feedbacks.deleted_at',
            'feedbacks.created_at',
            'feedbacks.updated_at',
            'IFNULL(feedbacks.first_name, user_details.first_name) AS first_name',
            'IFNULL(feedbacks.last_name, user_details.last_name) AS last_name',
            'IFNULL(feedbacks.avatar, users.avatar) AS avatar',
            'feedback_translations.id as feedback_translation_id',
            'feedback_translations.locale',
            'feedback_translations.rank',
            'feedback_translations.text',
            'feedback_translations.created_at',
            'feedback_translations.updated_at'
        ];

        $this->model = $this->model->selectRaw(implode(', ', $selectCols));

        if ($orderColumns !== null && $defOrderColumnKey !== null) {
            $orderColumn = $orderColumns[request()->input('order.0.column')] ?? $orderColumns[$defOrderColumnKey];
        }

        $this->model = $this->model->join('feedback_translations', 'feedbacks.id', '=', 'feedback_translations.feedback_id')
            ->leftJoin('users', 'feedbacks.user_id', '=', 'users.id')
            ->leftJoin('user_details', 'feedbacks.user_id', '=', 'user_details.user_id');

        if ($orderColumn !== null) {
            $this->model = $this->model->orderBy($orderColumn, $dir)
                ->when($orderColumn === 'first_name', function ($model) use ($dir) {
                    return $model->orderBy('last_name', $dir);
                })
                ->orderBy($orderColumns[$defOrderColumnKey], 'asc');
        }

        return parent::getDataforDataTables($searchColumns, null, null, $with, $where);
    }

    /**
     * Build the data index of the return array for the JS DataTables plugin
     *
     * @param Collection|array $collection
     * @return array
     */
    public function getDataIndexforDataTables($collection)
    {
        $data = [];

        foreach ($collection as $item) {
            $data[$item->id] = [
                'id' => $item->id,
                'full_name' => trim($item->first_name . ' '. $item->last_name),
                'avatar' => $item->avatar,
                'rank' => $item->rank,
                'rating' => $item->rating,
                'text' => $item->text,
                'groups' => implode(',', $item->groups->pluck('name')->all()),
                'visibility' => $item->visibility ? 'visibile' : 'hidden'
            ];
        }

        return array_values($data);
    }

    /**
     * Update or create a feedback instance
     *
     * @param array $input
     * @param string $key
     * @param bool $toArray
     *
     * @return Model|array|bool
     * @throws EloquentRepositoryException
     */
    public function updateOrCreate($input, $key = 'id', $toArray = false)
    {
        if (!is_array($input) || empty($input)) {
            return false;
        }

        if (! empty($input[$key])) {
            $feedback = $this->model->firstOrNew([$key => $input[$key]]);
        } else {
            $feedback = $this->getModel();
        }

        $feedbackTranslatable = $feedback->translateOrNew($input['locale']);
        $feedbackTranslatable->text = $input['text'];
        $feedbackTranslatable->rank = $input['rank'];

        unset($input['text'], $input['rank']);

        $feedback->fill($input)
            ->save();

        return $feedback;
    }
}
