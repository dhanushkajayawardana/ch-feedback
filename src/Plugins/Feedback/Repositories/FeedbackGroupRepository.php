<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 24/7/19
 * Time: 6:27 PM
 */

namespace Creativehandles\ChFeedback\Plugins\Feedback\Repositories;


use Creativehandles\ChFeedback\Plugins\Feedback\Model\FeedbackGroupModel;
use App\Repositories\BaseEloquentRepository;

class FeedbackGroupRepository extends BaseEloquentRepository
{
    public function __construct(FeedbackGroupModel $model)
    {
        $this->model = $model;
    }
}