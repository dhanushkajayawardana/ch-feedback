<?php

namespace Creativehandles\ChFeedback\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\FeedbackResource;
use App\Services\ExternalApiService;
use Creativehandles\ChFeedback\Plugins\Feedback\Feedback;
use Illuminate\Http\Request;

class FeedbackAPIController extends Controller
{
    /**
     * @var ExternalApiService
     */
    private $apiService;

    /**
     * @var Feedback
     */
    private $feedback;


    public function __construct(Request $request, ExternalApiService $apiService,Feedback $feedback )
    {
        if ($request->headers->has('client-locale')) {
            app()->setLocale($request->headers->get('client-locale'));
        }

        $this->apiService = $apiService;
        $this->feedback = $feedback;
    }


    /**
     * Get all feedback
     *
     * @param  Request  $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getAllFeedback(Request $request)
    {
        $relations = $request->get('with', []);
        $visibility = $request->get('visibility', null);
        $limit = $request->get('limit', 10);
        $skip = $request->get('skip', 0);

        $feedback = $this->feedback->getAllFeedbacks($relations);

        if ($visibility == '1') {
            $feedback = $feedback->filter(function ($item) {
                return $item->visibility == 1;
            });
        }

        if ($visibility == '0') {
            $feedback = $feedback->filter(function ($item) {
                return $item->visibility == 0;
            });
        }

        if ($limit) {
            $feedback = $feedback->slice($skip,$limit);
        }

        return $this->apiService->processResponse($feedback, FeedbackResource::class, true, $relations);
    }

    /**
     * Get a feedback
     *
     * @param  Request  $request
     * @param $id
     * @bodyParam with[0] array relationships ["tags"] Example: tags
     * @bodyParam with[1] array relationships ["categories"] Example: categories
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getFeedback(Request $request, $id)
    {
        $relations = $request->get('with', []);
        $feedback = $this->feedback->getFeedbackById($id);

        return $this->apiService->processResponse($feedback, FeedbackResource::class, false, $relations);
    }
}
