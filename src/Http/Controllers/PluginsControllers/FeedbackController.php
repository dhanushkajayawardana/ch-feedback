<?php

/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 24/7/19
 * Time: 2:22 PM
 */

namespace Creativehandles\ChFeedback\Http\Controllers\PluginsControllers;

use Log;
use App\User;
use Validator;
use App\Traits\UploadTrait;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Creativehandles\ChFeedback\Plugins\Feedback\Feedback;
use Creativehandles\ChFeedback\Plugins\Feedback\Model\FeedbackModel;
use Creativehandles\ChFeedback\Plugins\Feedback\Model\FeedbackGroupModel;
use Creativehandles\ChFeedback\Plugins\Feedback\Repositories\FeedbackRepository;

class FeedbackController extends Controller
{

    use UploadTrait;

    protected $views = 'Admin.Feedback.';
    protected $logprefix = "FeedbackController";
    /**
     * @var FeedbackRepository
     */
    private $feedbackRepository;

    /**
     * @var Feedback
     */
    private $feedback;

    public function __construct(Feedback $feedback, FeedbackRepository $feedbackRepository)
    {
        $this->feedback = $feedback;
        $this->feedbackRepository = $feedbackRepository;
    }

    public function index(Request $request)
    {
        return view($this->views . 'index', []);
    }

    public function create(Request $request)
    {
        $users = User::all();
        $groups = FeedbackGroupModel::all();
        return view($this->views . 'add')->with(compact('users', 'groups'));
    }

    public function edit(Request $request, $id)
    {
        $users = User::all();

        $groups = FeedbackGroupModel::all();

        try {
            $feedback = FeedbackModel::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            if ($request->ajax()) {
                return response()->json(['msg' => [__('references.404')]], 404);
            } else {
                throw $exception;
            }
        }

        if ($request->ajax()) {
            $feedback->setDefaultLocale($request->get('form_locale', app()->getLocale()));
            return response()->json(['data' => $feedback], 200);
        }

        return view($this->views . 'add', [
            'model' => $feedback,
            'users' => $users,
            'groups' => $groups,

        ]);
    }

    public function show(Request $request, $id)
    {
    }

    public function grid(Request $request)
    {
        $searchColumns = ['feedback_translations.text', 'feedbacks.first_name', 'feedbacks.last_name', 'user_details.first_name', 'user_details.last_name'];
        $searchValue = request()->input('search')['value'];
        $orderColumns = [
            '1' => 'first_name',
            '3' => 'feedback_translations.rank',
            '4' => 'feedbacks.rating',
            '5' => 'feedback_translations.text',
            '6' => 'feedback_group.name',
            '7' => 'feedbacks.visibility'
        ];
        $where = isset($searchValue) && $searchValue !== '' ? ['feedback_translations.locale', '=', app()->getLocale()] : null;
        $data = $this->feedbackRepository->getDataforDataTables($searchColumns, $orderColumns, '1', ['groups'], $where);

        $data['data'] = array_map(function ($item) {
            return [
                $item['id'],
                $item['full_name'],
                $item['avatar'],
                $item['rank'],
                $item['rating'],
                $item['text'],
                $item['groups'],
                $item['visibility']
            ];
        }, $data['data']);

        echo json_encode($data);
    }


    public function update(Request $request)
    {
        $rules = [
            'user_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'text' => 'required',
            'rank' => 'required',
        ];
        $userId = $request->get('user_id');
        $firstName = $request->get('first_name');
        $lastName = $request->get('last_name');

        if ($userId) {
            unset($rules['first_name']);
            unset($rules['last_name']);
        } else {
            if ($firstName && $lastName) {
                unset($rules['user_id']);
            }
        }


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            Log::error($this->logprefix . '(' . __FUNCTION__ . '): Feedback data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        //format and save basic user
        $feedbackData = [
            'locale' => $request->form_locale,
            'text' => $request->text,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'user_id' => $request->user_id,
            'rating' => $request->rating ?? 0,
            'rank' => $request->rank,
            'visibility' => (bool) $request->visibility ?? 0,
        ];

        if (!empty($feedbackData['user_id'])) {
            $feedbackData['first_name'] = $feedbackData['last_name'] = null;
        }

        $groups = $request->input('groups', []);

        foreach ($groups as $key => $group) {
            if (is_string($group)) {
                $storedGroup = $this->feedback->createNewGroup(['name' => $group]);
                $groups[$key] = $storedGroup->id;
            }
        }

        if ($request->id > 0) {
            $feedback = FeedbackModel::findOrFail($request->id);
            $feedbackData['id'] = $feedback->id;
            $storedFeedback = $this->feedback->createOrUpdateFeedback($feedbackData, 'id');
        } else {
            $storedFeedback = $this->feedback->createOrUpdateFeedback($feedbackData);
        }

        if ($request->has('avatar')) {
            // Get image file
            $image = $request->file('avatar');
            // set image path
            $fullPath = sprintf('/uploads/images/feedback/%s/avatar/', $storedFeedback->id);
            $imgPath = $this->UploadAvatar($image, $fullPath);
            $feedbackData['id'] = $storedFeedback->id;
            $feedbackData["avatar"] = $imgPath;
            $this->feedback->createOrUpdateFeedback($feedbackData, 'id');
        }


        //sync feedback categories
        if ($groups) {
            $storedFeedback->groups()->sync($groups);
        }

        if ($request->ajax()) {
            return response()->json(['msg' => "Feedback Saved", 'data' => $storedFeedback], 200);
        } else {
            return redirect()->route('admin.feedback')->with('success', 'Feedback added successfully!');
        }
    }

    public function store(Request $request)
    {
        return $this->update($request);
    }

    public function destroy(Request $request, $id)
    {

        $instructor = FeedbackModel::findOrFail($id);
        $instructor->delete();

        return response()->json(['msg' => "Feedback Deleted"], 200);
    }


    public function getAllFeedback()
    {
        $result = $this->feedback->getAllFeedbacks();

        if ($result)
            return $result;

        return [];
    }

    public function getAllFeedbackByVisibility($visibility)
    {
        $result = $this->feedback->getAllFeedbackByVisibility($visibility);

        if ($result)
            return $result;

        return [];
    }

    public function getFeedbackByGroup($group = null)
    {
        $result = $this->feedback->getFeedbackByGroup($group);

        if ($result)
            return $result;

        return [];
    }

    public function getFeedbackById($id)
    {
        $result = $this->feedback->getFeedbackById($id);

        if ($result)
            return $result;

        return [];
    }

    /**
     * UploadBolgImage function
     *
     * @param UploadedFile $uploadedFile
     * @param string $folder
     * @param string $name
     * @return array $file_path_list
     */
    public function UploadAvatar(UploadedFile $uploadedFile, $folder)
    {
        $quality   = config('ch-feedback.imageoptimaization.img_quality');
        $format    = config('ch-feedback.imageoptimaization.img_format');
        $size_arr  = config('ch-feedback.imageoptimaization.avatar');

        return $this->optimizedImageUploader($uploadedFile, $size_arr, $quality, $format, $folder);
    }
}
