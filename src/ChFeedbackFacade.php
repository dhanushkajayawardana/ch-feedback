<?php

namespace Creativehandles\ChFeedback;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\ChFeedback\Skeleton\SkeletonClass
 */
class ChFeedbackFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ch-feedback';
    }
}
