@extends('Admin.layout')
@section('styles')

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/forms/toggle/switchery.min.css")}}">
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @if (isset($model))
                        @include('Admin.partials.breadcumbs',['header' => 'test','params' => $model])
                    @else
                        @include('Admin.partials.breadcumbs',['header'=>__('general.Create Feedback')])
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('Admin.partials.form-alert')

    <div class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        @if (isset($model))
                            <h4 class="card-title" id="basic-layout-form-center">{{__('references.Update Feedback')}}</h4>
                        @else
                            <h4 class="card-title" id="basic-layout-form-center">{{__('references.Create new feedback')}}</h4>
                        @endif
                    </div>
                    <div class="card-content show">
                        <div class="card-body">
                            @if(isset($model))
                            <form action="{{ route('admin.feedback.update',['feedback'=>$model->id]) }}" method="POST"
                                  class="form-horizontal" enctype="multipart/form-data">
                                @else
                                    <form action="{{route('admin.feedback.store')}}" method="POST"
                                          class="form-horizontal" enctype="multipart/form-data">
                                @endif
                                {{ csrf_field() }}

                                @if (isset($model))
                                    <input type="hidden" name="_method" value="PATCH">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="hidden" name="id" id="id" class="form-control" readonly="readonly" value="{{$model->id}}">
                                        </div>
                                    </div>
                                    @else
                                            <input type="hidden" name="id" id="id" class="form-control" readonly="readonly">
                                @endif

                                <div class="form-group">
                                    <label for="user_id" class="col-sm-3 control-label">{{__('references.User')}}</label>
                                    <div class="col-sm-12">
                                        <select  name="user_id" id="user_id" class="form-control" data-placeholder="select a user">
                                            <option value="0" disabled selected>select a user</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}" {{ isset($model) ? ($model->user_id == $user->id) ? 'selected' : '' :''}}>{{$user->first_name}} {{$user->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="new-user-container">
                                <div class="form-group">
                                    <label for="first_name" class="col-sm-3 control-label">{{__('references.First Name')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="first_name" id="first_name" class="form-control" value="{{isset($model) ? $model->first_name : '' }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-sm-3 control-label">{{__('references.Last Name')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="last_name" id="last_name" class="form-control" value="{{isset($model) ? $model->last_name : '' }}">
                                    </div>
                                </div>

                                @php
                                 if (isset($model)) {
                                    $avatarArray = json_decode($model->avatar, true);
                                    if (is_array($avatarArray)) {
                                        $img_path = $avatarArray["200x200"];
                                    }
                                    else {
                                        $img_path = $model->avatar;
                                    }
                                }
                                @endphp
                                <div class="form-group">
                                    <label for="avatar" class="col-sm-3 control-label">{{__('references.Avatar')}}</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" class="form-control"
                                               name="avatar">

                                        @if(isset($model))
                                            <input  type="hidden" class="form-control" name="featured_image_old" value="{{$img_path}}">
                                            <img id="image_preview" src="{{asset($img_path)}}" alt="featured" class="height-150 img-thumbnail">
                                        @else
                                            <img id="image_preview" class="hide height-150 img-thumbnail" >
                                        @endif
                                    </div>
                                </div>
                                </div>
                                <div class="form-group">
                                    <label for="rank" class="col-sm-3 control-label">{{__('references.Rank')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="rank" id="rank" class="form-control" value="{{isset($model) ? $model->rank : '' }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rating" class="col-sm-3 control-label">{{__('references.Rating')}}</label>

                                    <div class="col-sm-12">
                                        <div id="default-star-rating" data-score="{{ isset($model) ? $model->rating : 0 }}" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="groupSelector" class="col-sm-3 control-label">{{__('references.Feedback group')}}</label>
                                    <div class="col-sm-12">
                                        <select class=" form-control" id="groupSelector"
                                                multiple="multiple" aria-hidden="true"
                                                name="groups[]">
                                            @foreach($groups as $group)
                                                @if(isset($model))
                                                    <option {{ in_array($group->name,$model->groups->pluck('name')->all()) ? 'selected' : '' }} value="{{$group->name}}">{{$group->name}}</option>
                                                @else
                                                    <option {{ (is_array(old('groups')) && in_array($group->name, old('groups')))? 'selected' :'' }}  value="{{$group->name}}">{{$group->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @include('Admin.partials.multi-language-select-locale', [
                                    'model' => isset($model) ? $model : null,
                                    'setRowCol' => false,
                                    'labelClass' => 'col-sm-3 control-label',
                                    'formElemInDiv' => true,
                                    'formElemDivClass' => 'col-sm-12'
                                ])
                                <div class="form-group">
                                    <label for="first_name" class="col-sm-3 control-label">{{__('references.Feedback')}}</label>
                                    <div class="col-sm-12">
                                        <textarea required type="text" name="text" id="text" class="form-control" >{{isset($model) ? $model->text : '' }}</textarea>
                                    </div>
                                </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">{{__('references.Feedback visibility')}}</label>
                                        <div class="col-md-12 custom-control ">
                                            <input type="checkbox" class="js-switch" {{ ((isset($model) ? $model->visibility : (old('visibility')) ) ?  'checked' : '' )}} name="visibility" id="visibility" />
                                            <label class="" for=""></label>
                                        </div>
                                    </div>
                                        <div class="form-actions left">
                                            <button type="reset" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> {{__('trainings.general.reset')}}
                                            </button>

                                            <a href="{{ URL::previous() }}">
                                                <button type="button" href="" class="btn btn-warning mr-1">
                                                    <i class="ft-arrow-left"></i> {{__('trainings.general.goBack')}}
                                                </button></a>
                                            <button type="button" class="btn btn-primary" id="saveForm">
                                                <i class="fa fa-check-square-o"></i> {{__('trainings.general.update')}}
                                            </button>
                                            {{--<button type="submit" class="btn btn-primary" id="saveAndGoBack">
                                                <i class="fa fa-check-square-o"></i> {{__('trainings.general.updateAndBack')}}
                                            </button>--}}
                                        </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
        window.STARPATH="{{asset('images/raty/')}}";
    </script>
    <script src="{{ asset("vendors/js/extensions/jquery.raty.js")}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script src="{{ asset("js/js/forms/toggle/bootstrap-checkbox.min.js")}}"></script>
    <script src="{{ asset("vendors/js/forms/toggle/switchery.min.js")}}"></script>
    <script src="{{ asset("js/scripts/feedback_scripts.js")}}"></script>
    <script>
        $(document).ready(function () {
            $('select[name="form_locale"]').on('change', function (e) {
                let slctLocale = $(this);
                let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                let id = slctLocale.closest('form').find('#id').val();
                let form_locale = slctLocale.val();
                @if(isset($model))
                let url = '{{ route('admin.feedback.edit', ['feedback' => $model->id]) }}';
                @else
                let url = '{{ url('admin/feedback') }}/' + id + '/edit';
                @endif

                if (id) {
                    $.ajax({
                        data: {_token: CSRF_TOKEN, id: id, form_locale: form_locale},
                        method: 'GET',
                        url: url,
                    }).done(function (object) {
                        try {
                            if (object.data.text) {
                                slctLocale.closest('form').find('textarea[name="text"]').val(object.data.text);
                            }

                            if (object.data.rank) {
                                slctLocale.closest('form').find('input[name="rank"]').val(object.data.rank);
                            }
                        } catch (e) {
                            console.log(e);
                        }
                    }).fail(function (error) {
                        console.log(error);
                        let messages = error.responseJSON.msg;

                        for (let message in messages) {
                            toastr.error(messages[message], 'Error', {timeOut: 5000});
                        }
                    });
                }
            });
        });
    </script>
@endsection
